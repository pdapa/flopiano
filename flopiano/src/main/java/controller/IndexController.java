/**
 * 
 */
package controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author Patrick DAPA
 *
 */
@Controller
public class IndexController {
	@RequestMapping(value = "/Connection", method = RequestMethod.GET)
	public String displayConnectionPage() {
		return "connection";
	}
}
