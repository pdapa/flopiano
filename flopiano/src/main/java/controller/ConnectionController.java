package controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import service.UserService;

@Controller
public class ConnectionController {

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/Connection", method = RequestMethod.POST)
	public String connection(ModelMap pModel, @Valid @ModelAttribute(value = "pseudo") final String pseudo,
			@Valid @ModelAttribute(value = "password") final String password) {

		String ihm = "connection";
		pModel.addAttribute("pseudo", pseudo);
		pModel.addAttribute("password", password);
		if (userService.isUserInDataBase(pseudo, password) == true) {
			ihm = "booking";
		}
		return ihm;
	}

	@RequestMapping(value = "/Connection", method = RequestMethod.POST)
	public String disConnection(ModelMap pModel) {
		pModel.addAttribute("pseudo", null);
		pModel.addAttribute("password", null);
		return "connection";
	}
}
