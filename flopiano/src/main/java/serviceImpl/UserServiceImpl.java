package serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dao.UserDAO;
import entity.User;
import service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDAO userDAO;

	/**
	 * {@inheritDoc}
	 */
	public boolean isUserInDataBase(String pseudo, String password) {
		User user = userDAO.findByCredentials(pseudo, password);
		return (user != null);
	}
}
