package dao;

import java.util.List;

import entity.User;

public interface UserDAO {
	/**
	 * Creates a user in the database.
	 */
	public abstract void createUser(User user);

	/**
	 * Update a user in the dababase.
	 * 
	 * @param user:user
	 *            given as parameter.
	 */
	public abstract void updateUser(User user);

	/**
	 * Deletes a given user in the database.
	 */
	public abstract void deleteUser(User user);

	/**
	 * @param id:
	 *            id of the given user
	 * @return a user entry from the database.
	 */
	public abstract User findById(Integer id);

	/**
	 * @return all the users present in the database
	 */
	public abstract List<User> findAll();

	/**
	 * 
	 * @param pseudo
	 *            : user pseudo.
	 * @param password:
	 *            user password
	 * @return user present in the database.
	 */
	public abstract User findByCredentials(String pseudo, String password);
}
