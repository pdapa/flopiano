package service;

public interface UserService {
	/**
	 * Check if the given user in present in the database.
	 * 
	 * @return {@link Boolean}
	 */
	public abstract boolean isUserInDataBase(String pseudo, String password);
}
