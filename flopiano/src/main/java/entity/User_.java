package entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2017-09-27T22:55:04.708+0200")
@StaticMetamodel(User.class)
public class User_ {
	public static volatile SingularAttribute<User, Integer> idUser;
	public static volatile SingularAttribute<User, String> name;
	public static volatile SingularAttribute<User, String> surname;
	public static volatile SingularAttribute<User, String> pseudo;
	public static volatile SingularAttribute<User, Integer> phone;
	public static volatile SingularAttribute<User, String> password;
	public static volatile SingularAttribute<User, Boolean> statusConnected;
}
