/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author Patrick DAPA
 */
@Table(name = "booking")
@Entity(name = "entity")
public class Booking implements Serializable {

	@Id
	@Column(name = "id_booking")
	private Integer idBooking;

	@Column(name = "event")
	private String event;

	@Column(name = "location")
	private String location;

	@Column(name = "date")
	private Date date;

	@Column(name = "message")
	private String message;

	@JoinColumn(nullable = false, unique = true, name = "user_id")
	@ManyToOne
	private User user;

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
