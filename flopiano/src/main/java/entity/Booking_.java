package entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2017-09-27T15:23:05.144+0200")
@StaticMetamodel(Booking.class)
public class Booking_ {
	public static volatile SingularAttribute<Booking, Integer> idBooking;
	public static volatile SingularAttribute<Booking, String> event;
	public static volatile SingularAttribute<Booking, String> location;
	public static volatile SingularAttribute<Booking, Date> date;
	public static volatile SingularAttribute<Booking, String> message;
	public static volatile SingularAttribute<Booking, User> user;
}
