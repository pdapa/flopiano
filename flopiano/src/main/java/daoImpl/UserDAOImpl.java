package daoImpl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import dao.UserDAO;
import entity.User;

@Repository
public class UserDAOImpl implements UserDAO {

	@PersistenceContext
	private EntityManager em;

	/**
	 * {@inheritDoc}
	 */
	public void createUser(User user) {
		em.getTransaction().begin();
		em.persist(user);
		em.getTransaction().commit();
	}

	/**
	 * {@inheritDoc}
	 */
	public void updateUser(User user) {
		em.getTransaction().begin();
		em.merge(user);
		em.getTransaction().commit();
	}

	/**
	 * {@inheritDoc}
	 */
	public void deleteUser(User user) {
		em.getTransaction().begin();
		em.remove(user);
		em.getTransaction().commit();
	}

	/**
	 * {@inheritDoc}
	 */
	public User findById(Integer id) {
		return em.find(User.class, id);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<User> findAll() {
		TypedQuery<User> query = em.createQuery("from entity.User", User.class);
		return query.getResultList();
	}

	/**
	 * {@inheritDoc}
	 */
	public User findByCredentials(String pseudo, String password) {
		TypedQuery<User> query = em
				.createQuery("from entity.User as user where user.pseudo = ?1 and user.password = ?2", User.class);
		query.setParameter(1, pseudo);
		query.setParameter(2, password);
		return query.getSingleResult();
	}

}
